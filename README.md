# Dockerfile for Harbour Project xBase compiler on Alpine Linux

This project holds **Dockerfile** for [Harbour Project](https://harbour.github.io/) compiler. Use it bo compile application for The Cross-Platform xBase compiler.

```bash
$ docker build -t harbour/alpine .
```

Issues found during builds on Alpine Linux: https://github.com/harbour/core/issues/176

Fixed in forked **harbour/core** repository: https://github.com/felixd/harbour-core [master]

![Harbour Project Logo](https://harbour.github.io/images/harbour.svg "Harbour Project Logo")


