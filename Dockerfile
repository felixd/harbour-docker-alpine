FROM alpine:latest as builder
RUN apk update && apk add --no-cache \
 git \
 gcc \
 make \
 openssl \
 libc-dev \
 zlib-dev \
 ncurses-dev \
 mariadb-dev \
 icu-dev \
 cairo-dev \
 minizip \
 libx11-dev \
 cups-dev \
 postgresql-dev \
 pcre2-dev \
 libmagic \
 sqlite-dev \
 ghostscript-dev \
 gd-dev \
 curl-dev \
 slang-dev

# Harbour Projecet - Core 3.2 version
RUN git clone --single-branch --depth 1 https://github.com/harbour/core.git hb32
# Viktor Szakats 3.4 version
RUN git clone --single-branch --depth 1 https://github.com/vszakats/harbour-core.git hb34
# Felix's version: 3.4.1
RUN git clone --single-branch --depth 1 https://gitlab.com/felixd/harbour.git hb341

ENV HB_WITH_OPENSSL=yes
ENV HB_BUILD_DYN=yes
ENV HB_BUILD_CONTRIB_DYN=yes

RUN cd /hb32 && make
RUN cd /hb34 && make
RUN cd /hb341 && make

FROM alpine:latest
COPY --from=builder /usr/local/ /usr/local/
RUN apk add --no-cache \
    bash \
    gcc \
    libc-dev \
    uncrustify \
    upx \
    make \
    ## needed for -static/-fullstatic
    zlib-dev

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/harbour/
